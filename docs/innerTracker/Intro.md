# IT middleware setup and use

## Recommended software and firmware versions

- Software git branch / tag : `Dev` / `v5-01`
- Firmware tag: `v4-09`

## Important webpages and information

- Mattermost forum: [`cms-it-daq`](https://mattermost.web.cern.ch/cms-it-daq/)
- Detailed description of the various calibrations: <https://cernbox.cern.ch/s/uSezc8ErG7F4tJ0>
- ROC tuning sequence: <https://www.overleaf.com/read/ffpkqnjjjscd>
- Latest IT-DAQ school: <https://indico.cern.ch/event/1374747/>
- CROC testing guide: <https://croc-testing-user-guide.docs.cern.ch/>
- Text-based User Interface (TUI) - aka Dirigent: <https://gitlab.cern.ch/ethz-phase2-pixels/dirigent/>
- Graphical-based User Interface (Ohio-GUI) - aka Dirigent: <https://github.com/OSU-CMS/Ph2_ACF_GUI/>
- Program to generate enable/injection patterns for x-talk studies: `pyUtilsIT/ManipulateITchipMask.py`
- Mask converter from `Ph2_ACF` to `Alki's` code: `pyUtilsIT/ConvertPh2ACFMask2Alkis.py`
