# Inner Tracker overview

1. [Introduction](Intro.md)
2. [FC7 setup](FC7setup.md)
3. [Firmware setup](FWsetup.md)
4. [Software setup](SWsetup.md)
5. [FPGA configuration](FPGAcfg.md)
